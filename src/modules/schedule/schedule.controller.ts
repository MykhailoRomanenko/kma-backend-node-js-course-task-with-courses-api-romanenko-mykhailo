import {Controller, Get, Param, UseInterceptors} from '@nestjs/common';
import {ScheduleService} from "./schedule.service";
import {AxiosErrorInterceptor} from "../../common/interceptors/axios-error.interceptor";
import {GetScheduleParamsDto} from "./dto/get-schedule.dto";
import {IScheduleItem} from "./types";
import {EmptyScheduleErrorInterceptor} from "./interceptors/empty-schedule.interceptor";

@Controller('schedule')
@UseInterceptors(AxiosErrorInterceptor, EmptyScheduleErrorInterceptor)
export class ScheduleController {
    constructor(
        protected readonly scheduleService: ScheduleService,
    ) {
    }

    @Get('/:year/:season')
    public async getSchedule(@Param() params: GetScheduleParamsDto): Promise<IScheduleItem[]> {
        return this.scheduleService.getSchedule(params)
    }
}
