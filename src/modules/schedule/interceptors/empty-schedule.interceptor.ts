import {CallHandler, ExecutionContext, Injectable, NestInterceptor, NotFoundException} from "@nestjs/common";
import {catchError, Observable, throwError} from "rxjs";
import {EmptyScheduleError} from "../errors/empty-schedule.error";

@Injectable()
export class EmptyScheduleErrorInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle().pipe(
            catchError((err: Error) => {
                if (err instanceof EmptyScheduleError) {
                    return throwError(() => new NotFoundException(err.message))
                }
                return throwError(() => err);
            })
        )
    }
}