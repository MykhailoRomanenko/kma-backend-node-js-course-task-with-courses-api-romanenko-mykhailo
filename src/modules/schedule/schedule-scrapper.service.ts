import {Injectable} from "@nestjs/common";
import cheerio, {Cheerio, CheerioAPI, Element} from "cheerio";
import {EducationLevel} from "../../common/types";
import {educationLevelAbbreviationToEnum} from "./util";
import {IScheduleItem} from "./types";

// these are used locally, so not in types.ts
interface IFileLevelScheduleItem {
    url: string,
    specialityName: string,
    updatedAt: string
}

interface IYearLevelScheduleItem extends IFileLevelScheduleItem {
    year: 1 | 2 | 3 | 4,
    level: EducationLevel
}

interface IFacultyLevelScheduleItem extends IYearLevelScheduleItem {
    facultyName: string
}

@Injectable()
export class ScheduleScrapperService {
    public scrapSchedule(schedulePage: string): Omit<IScheduleItem, 'season'>[] {
        const $: CheerioAPI = cheerio.load(schedulePage)

        // has an entry for each faculty
        const scheduleAccordion = $('#schedule-accordion')

        const scrapByFaculty = this.scrapByFaculty.bind(this)

        const scheduleItems: IFacultyLevelScheduleItem[] = []
        scheduleAccordion.children().each(function () {
            const moreItems = scrapByFaculty($, $(this))
            moreItems.forEach(e => scheduleItems.push(e))
        })

        return scheduleItems
    }

    private scrapByFaculty($: CheerioAPI, facultyPanel: Cheerio<Element>): IFacultyLevelScheduleItem[] {
        const facultyNameElem: Cheerio<Element> = facultyPanel.find('h4.panel-title').children(':first')
        const facultyName = facultyNameElem.text().trim()

        const scrapByYear = this.scrapByYear.bind(this)

        // has an entry for each year
        const facultyAccordion: Cheerio<Element> = facultyPanel.find('div.panel-body').first().children(':first')

        const scheduleItems: IFacultyLevelScheduleItem[] = []
        facultyAccordion.children().each(function () {
            const moreItems = scrapByYear($, $(this))
            moreItems.forEach(e => scheduleItems.push({...e, facultyName}))
        })
        return scheduleItems
    }

    private scrapByYear($: CheerioAPI, yearPanel: Cheerio<Element>): IYearLevelScheduleItem[] {
        const yearTitleElem = yearPanel.find('h4.panel-title').children(':first')
        const yearTitleSplit: string[] = yearTitleElem.text().trim().split(',')
        const level: EducationLevel = educationLevelAbbreviationToEnum(yearTitleSplit[0])
        const year: 1 | 2 | 3 | 4 = parseInt(yearTitleSplit[1]) as 1 | 2 | 3 | 4

        const scrapByFile = this.scrapByFile.bind(this)

        //has an entry for each schedule file
        const fileList = yearPanel.find('ul.list-group')

        const scheduleItems: IYearLevelScheduleItem[] = []
        fileList.children().each(function () {
            const scheduleItem = scrapByFile($(this).children(':first'))
            scheduleItems.push({...scheduleItem, level, year})
        })

        return scheduleItems
    }

    private scrapByFile(fileContainer: Cheerio<Element>): IFileLevelScheduleItem {
        const fileTitleElem = fileContainer.children(':last')

        const fileTitleText = fileTitleElem.text()
        let splitIndex = fileTitleText.indexOf('БП-')
        if (splitIndex === -1) {
            splitIndex = fileTitleText.indexOf('МП-')
        }
        const specialityName = fileTitleText.substring(0, splitIndex).trim()

        const url: string = fileTitleElem.attr('href') || 'URL not found.'

        const updatedAtText = fileContainer.children(':first').text()
        const startIndex: number = updatedAtText.indexOf(':') + 2
        const endIndex: number = updatedAtText.indexOf(')')
        const updatedAt = updatedAtText.substring(startIndex, endIndex)

        return {updatedAt, url, specialityName}
    }
}