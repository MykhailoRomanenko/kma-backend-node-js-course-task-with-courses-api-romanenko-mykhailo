import {CourseSeason, EducationLevel} from "../../common/types";

export function transformSeasonTextToEnum(season: string): CourseSeason | undefined {
    const seasonLowerCase: string = season.toLowerCase()
    switch (seasonLowerCase) {
        case CourseSeason.AUTUMN.toLowerCase():
            return CourseSeason.AUTUMN
        case CourseSeason.SPRING.toLowerCase():
            return CourseSeason.SPRING
        case CourseSeason.SUMMER.toLowerCase():
            return CourseSeason.SUMMER
    }
}

export function educationLevelAbbreviationToEnum(level: string): EducationLevel {
    switch (level) {
        case 'БП':
            return EducationLevel.BACHELOR
        case 'МП':
            return EducationLevel.MASTER
        default:
            throw new Error('Unknown education level')
    }
}

export function reformatUkrDateTimeStringToInverseDate(ukrDateString: string): string {
    const dateTextSplit: string[] = ukrDateString.split(' ')
    const dateVals: string[] = dateTextSplit[0].split('.')
    const timeVals: string = dateTextSplit[1]
    return `${dateVals.reverse().join('-')} ${timeVals}`
}