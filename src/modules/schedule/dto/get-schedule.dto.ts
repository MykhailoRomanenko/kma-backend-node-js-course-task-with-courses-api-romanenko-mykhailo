import {IsNumberString, Matches} from "class-validator";
import {CourseSeason} from "../../../common/types";
import {Transform, Type} from "class-transformer";
import {transformSeasonTextToEnum} from "../util";

export class GetScheduleParamsDto {
    @IsNumberString()
    year!: number

    @Type(()=>String)
    @Transform(({value}) => transformSeasonTextToEnum(value))
    @Matches(`^${Object.values(CourseSeason).join('|')}$`, 'i',
        {message: "Season must be 'spring', 'summer' or 'autumn'"})
    season!: CourseSeason
}