import { Module } from '@nestjs/common';
import { ScheduleController } from './schedule.controller';
import {ScheduleService} from "./schedule.service";
import {ScheduleScrapperService} from "./schedule-scrapper.service";

@Module({
  imports: [],
  controllers: [ScheduleController],
  providers: [ScheduleService, ScheduleScrapperService],
})
export class ScheduleModule {}
