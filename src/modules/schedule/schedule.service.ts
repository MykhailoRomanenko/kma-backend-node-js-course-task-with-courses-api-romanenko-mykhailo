import {Injectable} from "@nestjs/common";
import {GetScheduleParamsDto} from "./dto/get-schedule.dto";
import axios from "axios";
import {CourseSeason} from "../../common/types";
import {reformatUkrDateTimeStringToInverseDate} from "./util";
import {IScheduleItem} from "./types";
import {ScheduleScrapperService} from "./schedule-scrapper.service";
import {EmptyScheduleError} from "./errors/empty-schedule.error";


@Injectable()
export class ScheduleService {
    constructor(private readonly scheduleScrapperService: ScheduleScrapperService) {
    }

    public async getSchedule(params: GetScheduleParamsDto): Promise<IScheduleItem[]> {
        let academicYear = params.year
        if (params.season !== CourseSeason.AUTUMN) {
            --academicYear
        }
        const seasonIndex = Object.values(CourseSeason).indexOf(params.season) + 1

        const schedulePage: string = (await axios(`https://my.ukma.edu.ua/schedule/?year=${academicYear}&season=${seasonIndex}`)).data

        const data: IScheduleItem[] = this.scheduleScrapperService.scrapSchedule(schedulePage).map((e) => {
            e.updatedAt = reformatUkrDateTimeStringToInverseDate(e.updatedAt)
            return {...e, season: params.season}
        })

        if (data.length === 0) {
            throw new EmptyScheduleError('No records in schedule.')
        }

        return data
    }


}