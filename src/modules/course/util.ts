import {CourseSeason, EducationLevel} from "../../common/types";

export function educationLevelTextToEnum(level: string): EducationLevel {
    switch (level) {
        case 'Бакалавр':
            return EducationLevel.BACHELOR
        case 'Магістр':
            return EducationLevel.MASTER
        default:
            throw new Error('Unable to resolve education level')
    }
}

export function seasonTextToEnum(season: string): CourseSeason {
    if (season.endsWith('д')) {
        return CourseSeason.SUMMER
    }
    if (parseInt(season) % 2 === 0) {
        return CourseSeason.SPRING
    }
    return CourseSeason.AUTUMN
}