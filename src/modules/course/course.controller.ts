import {Controller, Get, Param, UseInterceptors} from '@nestjs/common';
import {CourseService} from "./course.service";
import {AxiosErrorInterceptor} from "../../common/interceptors/axios-error.interceptor";
import {ICourse} from "./types";
import {GetCourseParamsDto} from "./dto/get-course.dto";



@Controller('course')
@UseInterceptors(AxiosErrorInterceptor)
export class CourseController {
    constructor(
        protected readonly courseService: CourseService,
    ) {
    }

    @Get(':code')
    public async getCourse(@Param() params: GetCourseParamsDto): Promise<ICourse> {
        return this.courseService.getCourse(params.code);
    }
}
