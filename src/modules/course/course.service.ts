import {Injectable} from '@nestjs/common';
import axios from "axios";
import cheerio, {Cheerio, Element} from 'cheerio'
import {CourseSeason, EducationLevel} from "../../common/types";
import {ICourse} from "./types";
import {educationLevelTextToEnum, seasonTextToEnum} from "./util";

@Injectable()
export class CourseService {
    private scrapCourse(coursePage: string): ICourse {
        const $ = cheerio.load(coursePage)

        const codeElem: Cheerio<Element> = $('th:contains("Код")').first().next()
        const code: number = parseInt(codeElem.text())

        const name: string = $('h1').first().text().trim().split('\t')[0]

        const descriptionElem: Cheerio<Element> = $('button:contains("Перегляд анотації")').first().next()
        let description: string | undefined
        if (descriptionElem.length) {
            description = descriptionElem.text().trim()
        }

        const facultyNameElem: Cheerio<Element> = $('th:contains("Факультет")').first().next()
        const facultyName: string = facultyNameElem.text().trim()

        const departmentNameElem: Cheerio<Element> = $('th:contains("Кафедра")').first().next()
        const departmentName: string = departmentNameElem.text().trim()

        const levelElem: Cheerio<Element> = $('th:contains("Освітній рівень")').first().next()
        const level: EducationLevel = educationLevelTextToEnum(levelElem.text().trim())

        const teacherNameElem: Cheerio<Element> = $('th:contains("Викладач")').first().next()
        const teacherName: string = teacherNameElem.text().trim()

        const creditsElem: Cheerio<Element> = $('th:contains("Інформація")').first().next().children(':first')
        const creditsAmount: number = parseFloat(creditsElem.text().split(' ')[0])

        const hoursElem: Cheerio<Element> = creditsElem.next()
        const hoursAmount: number = parseInt(hoursElem.text().split(' ')[0])

        const yearElem: Cheerio<Element> = hoursElem.next()
        const year: 1 | 2 | 3 | 4 = parseInt(yearElem.text().split(' ')[0]) as 1 | 2 | 3 | 4

        const seasonsElem: Cheerio<Element> = $('th:contains("Семестри")').first().next()
        const seasons: CourseSeason[] = []
        seasonsElem.children().each(function () {
            const seasonText = $(this).text().split(' ')[1]
            seasons.push(seasonTextToEnum(seasonText))
        })

        return {
            code,
            name,
            description,
            facultyName,
            departmentName,
            level,
            year,
            seasons,
            creditsAmount,
            hoursAmount,
            teacherName
        }
    }

    public async getCourse(code: number): Promise<ICourse> {
        const coursePage: string = (await axios(`https://my.ukma.edu.ua/course/${code}`)).data
        return this.scrapCourse(coursePage)
    }
}
