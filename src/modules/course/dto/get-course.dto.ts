import {IsNumberString} from "class-validator";

export class GetCourseParamsDto {
    @IsNumberString()
    code!: number
}