import {CallHandler, ExecutionContext, HttpException, Injectable, NestInterceptor} from "@nestjs/common";
import {catchError, Observable, throwError} from "rxjs";
import axios, {AxiosError} from "axios";

@Injectable()
export class AxiosErrorInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle().pipe(
            catchError((err: Error | AxiosError) => {
                if (axios.isAxiosError(err)) {
                    if (err.response) {
                        return throwError(() => new HttpException('Axios error', err.response!.status))
                    }
                }
                return throwError(() => err);
            })
        )
    }
}